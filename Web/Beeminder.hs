{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}

module Web.Beeminder (
  call,
  call',
  callWithResponse,
  -- Re-exports from Web.Beeminder.Api
  getGoal,
  getGoalDataPoints,
  basicDataPoint,
  createDataPoint,
  createDataPointDefaults,
  createMultipleDataPoints,
  updateDataPoint,
  updateDataPointDefaults,
  CreateDataPointParams(..),
  DataPointParams(..),
  UpdateDataPointParams(..),
  -- Re-exports from Web.Beeminder.Types
  UserId(..),
  GoalId(..),
  DataPointId(..),
  Timestamp(..),
  Goal(..),
  Burner(..),
  GoalType(..),
  DataPoint(..),
  RoadPoint(..),
  Road(..),
  APIRequest, -- hide constructors
  BeeminderError(..),
) where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.Catch
import Control.Monad.Trans
import Control.Monad.Trans.Control
import Control.Monad.Trans.Resource
import qualified Data.ByteString.Char8 as C
import Data.Aeson
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Scientific
import Data.Typeable
import Network.HTTP.Conduit
import Web.Authenticate.OAuth
import Web.Beeminder.Api
import Web.Beeminder.Types

endpoint :: String
endpoint = "https://www.beeminder.com/api/v1"

getReq :: MonadResource m => Credential -> APIRequest apiName responseType -> m Request
getReq auth_token apiReq = case apiReq of
  (APIRequestGet url) -> do
    req <- parseUrl $ endpoint ++ url
    return $ setQueryString credentialAsParams
      req { method = "GET" }
  (APIRequestPost url params) -> do
    req <- parseUrl $ endpoint ++ url
    return $ setQueryString (credentialAsParams ++ params)
      req { method = "POST" }
  (APIRequestPut url params) -> do
    req <- parseUrl $ endpoint ++ url
    return $ setQueryString (credentialAsParams ++ params)
      req { method = "PUT" }
  (APIRequestDelete url) -> do
    req <- parseUrl $ endpoint ++ url
    return $ setQueryString credentialAsParams
      req { method = "DELETE" }
  where
  mapSnd f (a, b) = (a, f b)
  credentialAsParams = (map (mapSnd Just) $ unCredential auth_token)

call :: (MonadIO m, MonadBaseControl IO m, MonadThrow m, FromJSON responseType)
  => Credential -> APIRequest apiName responseType -> m responseType
call cred apiReq = withManager $ \m -> call' cred m apiReq

call' :: (MonadResource m, FromJSON responseType)
  => Credential -> Manager -> APIRequest apiName responseType -> m responseType
call' cred m apiReq = responseBody <$> callWithResponse cred m apiReq

callWithResponse :: (MonadResource m, FromJSON responseType)
  => Credential -> Manager -> APIRequest apiName responseType -> m (Response responseType)
callWithResponse cred m apiReq = do
  req <- getReq cred apiReq
  res <- httpLbs req m
  case eitherDecodeStrict $ toStrict $ responseBody res of
    Right x -> return $ res {responseBody = x}
    Left err -> monadThrow $ FromJSONError err
  where
    mapSnd f (a,b) = (a, f b)
