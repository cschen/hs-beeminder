{-# LANGUAGE DeriveDataTypeable, OverloadedStrings #-}
module Web.Beeminder.Types where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Data.Aeson
import Data.Attoparsec.Number
import qualified Data.Aeson.Types
import Data.ByteString (ByteString)
import Data.Scientific
import Data.Text
import Data.Typeable
import qualified Data.Vector as V

newtype UserId = UserId {getUserId :: String} deriving (Eq, Ord, Show)
instance FromJSON UserId where parseJSON v = UserId <$> parseJSON v

newtype GoalId = GoalId {getGoalId :: String} deriving (Eq, Ord, Show)
instance FromJSON GoalId where parseJSON v = GoalId <$> parseJSON v

newtype DataPointId = DataPointId {getDataPointId :: String} deriving (Eq, Ord, Show)
instance FromJSON DataPointId where parseJSON v = DataPointId <$> parseJSON v

newtype Timestamp = Timestamp {fromTimestamp :: Integer} deriving (Eq, Ord, Show)
instance FromJSON Timestamp where parseJSON v = Timestamp <$> parseJSON v
instance ToJSON Timestamp where toJSON = toJSON . fromTimestamp

data Goal = Goal {
  gSlug :: GoalId,
  gUpdatedAt :: Timestamp,
  gBurner :: Burner,
  gTitle :: String,
  gGoalDate :: Maybe Timestamp,
  gGoalVal :: Maybe Number,
  gRate :: Maybe Number,
  gGraphUrl :: String,
  gThumbUrl :: String,
  gAutodata :: Maybe String,
  gGoalType :: GoalType,
  -- TODO: losedate ... contract [https://www.beeminder.com/api]
  gRoad :: Road
} deriving Show

instance FromJSON Goal where
  parseJSON (Object v) = Goal <$>
    v .: "slug" <*>
    v .: "updated_at" <*>
    v .: "burner" <*>
    v .: "title" <*>
    v .: "goaldate" <*>
    v .: "goalval" <*>
    v .: "rate" <*>
    v .: "graph_url" <*>
    v .: "thumb_url" <*>
    v .: "autodata" <*>
    v .: "goal_type" <*>
    v .: "roadall"

data RoadPoint  = TimeAndGoal Timestamp Number
                | GoalAndRate Number Number
                | TimeAndRate Timestamp Number
  deriving (Eq, Show)
type Road = [RoadPoint]
instance FromJSON RoadPoint where
  parseJSON (Array v) = case V.toList v of
    (t@(Number _) : g@(Number _) : Null : []) -> TimeAndGoal <$> parseJSON t <*> parseJSON g
    (Null : g@(Number _) : r@(Number _) : []) -> GoalAndRate <$> parseJSON g <*> parseJSON r
    (t@(Number _) : Null : r@(Number _) : []) -> TimeAndRate <$> parseJSON t <*> parseJSON r
instance ToJSON RoadPoint where
  toJSON (TimeAndGoal t g) = toJSON [toJSON t, toJSON g, Null]
  toJSON (GoalAndRate g r) = toJSON [Null, toJSON g, toJSON r]
  toJSON (TimeAndRate t r) = toJSON [toJSON t, Null, toJSON r]

data Burner = Frontburner | Backburner deriving (Eq, Show)
instance FromJSON Burner where
  parseJSON (String "frontburner") = pure $ Frontburner
  parseJSON (String "backburner") = pure $ Backburner

data GoalType = DoMore
              | DoLess
              | Odometer
              | LoseWeight
              | GainWeight
              | WhittleDown
              | Custom
  deriving (Eq, Show)
instance FromJSON GoalType where
  parseJSON (String "hustler") = pure $ DoMore
  parseJSON (String "drinker") = pure $ DoLess
  parseJSON (String "fatloser") = pure $ LoseWeight
  parseJSON (String "gainer") = pure $ GainWeight
  parseJSON (String "inboxer") = pure $ WhittleDown
  parseJSON (String "biker") = pure $ Odometer
  parseJSON (String "custom") = pure $ Custom

data DataPoint = DataPoint {
  dDataPointId :: DataPointId,
  dTimestamp :: Timestamp,
  dUpdatedAt :: Timestamp,
  dComment :: String,
  dValue :: Number,
  dRequestId :: Maybe String
  }
  deriving Show
instance FromJSON DataPoint where
  parseJSON (Object v) = DataPoint <$>
                          v .: "id" <*>
                          v .: "timestamp" <*>
                          v .: "updated_at" <*>
                          v .: "comment" <*>
                          v .: "value" <*>
                          v .: "requestid"
  parseJSON _   = mzero

type APIParams = [(ByteString, Maybe ByteString)]

data APIRequest apiName responseType =
  APIRequestGet {_url :: String}
  | APIRequestPost {_url :: String, _params :: APIParams}
  | APIRequestPut {_url :: String, _params :: APIParams}
  | APIRequestDelete {_url :: String}

data BeeminderError = FromJSONError String
  deriving (Show, Typeable)
instance Exception BeeminderError
