{-# LANGUAGE OverloadedStrings #-}

module Web.Beeminder.Api where

import Control.Applicative
import Data.Aeson
import Data.ByteString.Char8 hiding (map)
import Data.ByteString.Lazy (toStrict)
import Data.Maybe
import Data.Scientific
import Web.Beeminder.Types

data GetGoal
getGoal :: UserId -> GoalId -> APIRequest GetGoal Goal
getGoal user goal = APIRequestGet {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ ".json"
  }

data GetGoalDataPoints
getGoalDataPoints :: UserId -> GoalId -> APIRequest GetGoalDataPoints [DataPoint]
getGoalDataPoints user goal = APIRequestGet {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ "/datapoints.json"
  }

data UpdateGoal
updateGoal :: UserId -> GoalId -> UpdateGoalParams -> APIRequest UpdateGoal Value
updateGoal user goal params = APIRequestPut {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ ".json",
  _params =
    catMaybes [
      pairStringWithKey "slug" . getGoalId <$> ugpSlug params,
      pairStringWithKey "title" <$> ugpTitle params,
      pairStringWithKey "secret" . show <$> ugpSecret params,
      pairStringWithKey "datapublic" . show <$> ugpDataPublic params,
      pairBytestringWithKey "roadall" . toStrict . encode . toJSON <$> ugpRoad params
    ]
  }
  where
  pairStringWithKey k v = (k, Just $ pack v)
  pairBytestringWithKey k v = (k, Just v)

data UpdateGoalParams = UpdateGoalParams {
  ugpSlug :: Maybe GoalId,
  ugpTitle :: Maybe String,
  -- TODO: panic
  ugpSecret :: Maybe Bool,
  ugpDataPublic :: Maybe Bool,
  ugpRoad :: Maybe Road
}
updateGoalDefaults :: UpdateGoalParams
updateGoalDefaults = UpdateGoalParams Nothing Nothing Nothing Nothing Nothing

data DataPointParams = DataPointParams {
  dpTimestamp :: Maybe Timestamp,
  dpValue :: Scientific,
  dpComment :: String,
  dpRequestId :: Maybe String
}
instance ToJSON DataPointParams where
  toJSON datapoint = object $
    [("value", toJSON $ dpValue datapoint)] ++
    [("comment", toJSON $ dpComment datapoint)] ++
    catMaybes [
      (,) "timestamp" . toJSON <$> dpTimestamp datapoint,
      (,) "requestId" . toJSON <$> dpRequestId datapoint
    ]

data CreateDataPoint
data CreateDataPointParams = CreateDataPointParams {
  cdpSendMail :: Maybe Bool
}

createDataPoint :: UserId -> GoalId -> DataPointParams -> CreateDataPointParams -> APIRequest CreateDataPoint DataPoint
createDataPoint user goal datapoint params = APIRequestPost {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ "/datapoints.json",
  _params = 
    pairStringWithKey "value" (show $ dpValue datapoint) :
    pairStringWithKey "comment" (dpComment datapoint) :
    catMaybes [
      pairStringWithKey "timestamp" . show . fromTimestamp <$> dpTimestamp datapoint,
      pairStringWithKey "sendmail" . show <$> cdpSendMail params,
      pairStringWithKey "requestid" <$> dpRequestId datapoint
    ]
  }
  where
  pairStringWithKey k v = (k, Just $ pack v)

basicDataPoint :: Scientific -> DataPointParams
basicDataPoint x = DataPointParams {
  dpTimestamp = Nothing,
  dpValue = x,
  dpComment = "",
  dpRequestId = Nothing
}

createDataPointDefaults :: CreateDataPointParams
createDataPointDefaults = CreateDataPointParams {
  cdpSendMail = Nothing
}

data CreateMultipleDataPoints
data CreateMultipleDataPointsParams = CreateMultipleDataPointsParams {
  cmdpSendMail :: Maybe Bool
}

createMultipleDataPoints ::
    UserId ->
    GoalId ->
    [DataPointParams] ->
    CreateMultipleDataPointsParams ->
    APIRequest CreateMultipleDataPointsParams [DataPoint]
createMultipleDataPoints user goal datapoints params = APIRequestPost {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ "/datapoints/create_all.json",
  _params =
    maybeToList (pairStringWithKey "sendmail" . show <$> cmdpSendMail params)
    ++
    [("datapoints", Just $ datapointsJson)]
  }
  where
  pairStringWithKey k v = (k, Just $ pack v)
  datapointsJson = toStrict $ encode $ toJSON $ datapoints


data UpdateDataPoint
data UpdateDataPointParams = UpdateDataPointParams {
  udpTimestamp :: Maybe Timestamp,
  udpValue :: Maybe Scientific,
  udpComment :: Maybe String
}
updateDataPointDefaults = UpdateDataPointParams Nothing Nothing Nothing

updateDataPoint ::
    UserId ->
    GoalId ->
    DataPointId ->
    UpdateDataPointParams ->
    APIRequest UpdateDataPoint DataPoint
updateDataPoint user goal datapoint params = APIRequestPut {
  _url = "/users/"
    ++ (getUserId user)
    ++ "/goals/"
    ++ (getGoalId goal)
    ++ "/datapoints/"
    ++ (getDataPointId datapoint)
    ++ ".json",
  _params =
    catMaybes [
      pairStringWithKey "timestamp" . show . fromTimestamp <$> udpTimestamp params,
      pairStringWithKey "value" . show <$> udpValue params,
      pairStringWithKey "comment" <$> udpComment params
    ]
  }
  where
  pairStringWithKey k v = (k, Just $ pack v)
